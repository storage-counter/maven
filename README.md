# maven

There are two issues with Maven:

1. We allow duplicate uploads. This issue will address that https://gitlab.com/gitlab-org/gitlab/-/issues/10125
1. The package registry UI and usage quotas UI do not match up. I uploaded 6 packages @ 3.77 KiB each. I expect the usage quotas to show 22.62. It actually shows 25.24. This will need investigation.
